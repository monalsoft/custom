module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        clean: {
            build: ['build']
        },
        requirejs: {
            compile: {
                options: grunt.file.readJSON('source/js/build-config.json')
            }
        },
        copy: {
            main: {
                files: [
                    {
                        expand: true,
                        cwd: 'source/js/libs/',
                        src: ['**/*'],
                        dest: 'build/js/libs/'
                     }
                ]
            }
        },
        uglify: {
            main: {
                options: {
                    sourceMappingURL: './source-map.js',
                    sourceMap: 'build/js/source-map.js',
                    mangle: false
                },
                files: {
                    'build/js/main.js': ['build/js/main-src.js']
                }
            }
        },
        htmlmin: {
            partials: {
                options: {
                    removeComments: true,
                    collapseWhitespace: true
                },
                files: [{
                    expand: true,
                    dot: false,
                    cwd: 'source/partials',
                    dest: 'build/partials',
                    src: ['*.html', '*/*.html', '*/*/*.html']
                }]
            },
            html: {
                options: {
                    removeComments: true,
                    collapseWhitespace: true
                },
                files: [{
                    expand: true,
                    cwd: 'source/',
                    src: ['index.html'],
                    dest: 'build/'
                }]
            }
        },
        cssmin: {
            compress: {
                files: {
                    'build/css/style.css': [
                        'source/css/bootstrap.css',
                        'source/css/bootstrap-responsive.css',
                        'source/css/main.css'
                    ]
                }
            }
        },
        connect: {
            server: {
                options: {
                    port: 3000,
                    base: 'build'
                }
            }
        },
        watch: {
            cssFiles: {
                files: ["source/css/**/*"],
                tasks: ["cssmin"]
            },
            htmlFiles: {
                files: ["source/partials/**/*"],
                tasks: ["htmlmin:html"]
            },
            jsFiles: {
                files: ["source/js/**/*"],
                tasks: ["requirejs"]
            }
        }
    });

    // Load the plugin that provides ntasks
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-compress');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-connect');

    grunt.registerTask('default', ['clean', 'copy', 'requirejs', 'uglify', 'htmlmin', 'cssmin', 'connect', 'watch']);

};