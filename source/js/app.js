/**
 * loads sub modules and wraps them up into the main module
 * this should be used for top-level module definitions only
 * (in other words... you probably don't need to do stuff here)
 */
define([
    'angular',
    'config',
    'custom/custom',
    'custom/custom_auth',
    'custom/custom_data',
    'custom/custom_endpoint',
    'controllers/index',
    'directives/index',
    'filters/index',
    'services/index'
],
function (angular, config) {
    'use strict';
    var appName = 'app',
        application  = angular.module(
            appName,
            [
                'ngResource',
                'server-custom',
                'server-custom-data',
                'server-custom-endpoint',
                'server-custom-auth'
            ]);

    application.config(
        [
            '$routeProvider',
            '$controllerProvider',
            '$compileProvider',
            '$filterProvider',
            '$provide',
            function ($routeProvider, $controllerProvider, $compileProvider, $filterProvider, $provide) {
                application.register = {
                    controller: $controllerProvider.register,
                    directive: $compileProvider.directive,
                    filter: $filterProvider.register,
                    factory: $provide.factory,
                    service: $provide.service
                };
            }
        ]
    );

    // Alert angular break error
    application.factory('$exceptionHandler', function () {
        return function (exception, cause) {
            console.error('Error: ' + exception.message);
        };
    });

    return application;
}
);