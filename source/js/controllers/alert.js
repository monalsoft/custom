define(['app'], function (app) {
    "use strict";
    app.controller(
        'AlertCtrl',
        [
            '$scope',
            'Alert',
            function ($scope, Alert) {
                $scope.alerts = Alert.alerts;

                $scope.closeAlert = function ( index ) {
                    Alert.remove(index);
                };
            }
        ]
    );
});