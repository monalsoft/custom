﻿/**
 * defines constants for application
 */
define('config', function () {
    'use strict';
    return {
        templateVersion: '0.0.1',
        appName: 'Custom',
        homePages: {
            1: '/',
            2: 'admin/dashboard',
            4: '/activityfeed'
        },
        userInfoProjection: {lastname: 1, image_url: 1},
        servercustom: {
            env: 'live',//dev, live
            settings: function () {
                switch (this.env) {
                    case 'dev':
                        return {
                            dev: true,
                            appId: ''
                        };
                    case 'live':
                        return {
                            live: true,
                            appId: 'sdfds'
                        };
                }
            }
        }
    };
});