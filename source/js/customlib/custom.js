define(
    [
        'angular',
        'config',
        'customlib/cookies'
    ],
    function (angular, config) {
        'use strict';

        var custom = angular.module('server-custom', ['ngCookies']);

        custom.config(['$httpProvider', function ($httpProvider) {
            $httpProvider.defaults.useXDomain = true;
            delete $httpProvider.defaults.headers.common['X-Requested-With'];

            $httpProvider.responseInterceptors.push('loadingHttpInterceptor');
        }]);

        custom.factory(
            'loadingHttpInterceptor',
            [
                '$q',
                '$rootScope',
                function ($q, $rootScope) {

                    function success(response) {
                        $rootScope.isLoading--;
                        return response;
                    }

                    function error(response) {
                        $rootScope.isLoading--;

                        //response => {data: '', status: '', headers: '', config: ''}
                        switch (response.status) {
                            case 401:
                                $rootScope.$broadcast('request-error-401');
                                break;
                        }
                        return $q.reject(response);
                    }

                    return function (promise) {
                        if (!$rootScope.isLoading) {
                            $rootScope.isLoading = 0;
                        }
                        $rootScope.isLoading++;
                        return promise.then(success, error);
                    };
                }
            ]
        );

        custom.factory(
            'server-custom-settings',
            function () {
                var customUrl,
                    host = '',
                    port = '',
                    settings = config.servercustom.settings();

                if (settings.live) {
                    host = 'http://localhost';
                    
                   // customUrl = host + '/' + settings.appId;
                   customUrl = host;
                } else {
                    host = 'http://localhost';
                    port = '3301';
                    customUrl = host + ':' + port + '/' + settings.appId;
                }
                return {
                    env: settings.env,
                    afterLoginUri: config.afterLoginUri,
                    host: host,
                    port: port,
                    customUrl: customUrl
                };
            }
        );

        custom.factory(
            'server-custom-request',
            [
                '$http',
                '$rootScope',
                '$cookieStore',
                'server-custom-settings',
                function ($http, $rootScope, $cookieStore, settings) {

                    function toJson(obj, pretty) {
                        return JSON.stringify(obj, undefined, pretty ? '  ' : null);
                    }

                    return function (method, route, data, params, cb) {

                        var installId = $cookieStore.get('installId'),
                            token = $cookieStore.get('auth');

                        if (!cb && typeof params === 'function') {
                            cb = params;
                            params = null;
                        } else if (!cb && typeof data === 'function') {
                            cb = data;
                            data = null;
                            params = null;
                        }

                        if (token && typeof token === 'string') {
                            $http.defaults.headers.common.authorization = token;
                        }

                        if (installId) {
                            $http.defaults.headers.common['X-SS-custom-INSTALL-ID'] = installId;
                        }

                        var props = {
                            method: angular.uppercase(method),
                            url: settings.customUrl + '/' + route,
                            data: data,
                            params: params,
                            cache: false
                        };

                        // make angular to not skip object properties starting with $.
                        // we are using it while querying server-custom
                        if (data && data.query) {

                            props.transformRequest = [function (d) {
                                return angular.isObject(d) ? toJson(d) : d;
                            }];
                        }


                        $http(props)
                            .success(function (data) {
                                cb(null, data);
                            })
                            .error(function (data, status) {
                                switch (status) {
                                    case 0:
                                        cb('Can not connect with server', null);
                                        break;
                                    default:
                                        if (data.err) {
                                            var err = data.err;
                                            if (err.err) {
                                                cb(data.err);
                                            } else if (err.lastErrorObject) {
                                                cb(err.lastErrorObject.err);
                                            } else {
                                                cb(err);
                                            }
                                        } else {
                                            cb(data);
                                        }
                                }
                            });
                    };
                }
            ]
        );

        custom.factory('Alert', ['$rootScope', function ($rootScope) {
            $rootScope.alerts = [];
            return {
                alerts: $rootScope.alerts,
                success: function (msg) {
                    $rootScope.alerts.push({ type: 'success', msg: msg });
                    this.removeAfterInterval();
                },
                info: function (msg) {
                    $rootScope.alerts.push({ type: 'info', msg: msg});
                    this.removeAfterInterval();
                },
                warning: function (msg) {
                    $rootScope.alerts.push({ type: 'warning', msg: msg });
                    this.removeAfterInterval();
                },
                error: function (msg) {
                    $rootScope.alerts.push({ type: 'error alert-danger', msg: msg});
                    this.removeAfterInterval();
                },
                remove: function (index) {
                    $rootScope.alerts.splice(index, 1);
                },
                removeAfterInterval: function () {
                    var self = this,
                        idx = self.alerts.length - 1,
                        interval = 3000;

                    setTimeout(function (idx) {
                        $rootScope.alerts.splice(idx, 1);
                    }, interval);
                }
            };
        }]);

        return custom;
    }
);
