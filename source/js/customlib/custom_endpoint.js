define(['angular'], function (angular) {
    'use strict';

    var customEndpoint = angular.module('server-custom-endpoint', ['server-custom']);

    customEndpoint.factory(
        'server-custom-endpoint',
        [
            '$resource',
            'server-custom-request',
            'Alert',
            function ($resource, customRequest, Alert) {
                return {
                    get: function (endpointName, params, cb) {
                        var route = 'endpoint/' + endpointName;
                        customRequest('GET', route, null, params, cb);
                    },
                    post: function (endpointName, model, params, cb) {
                        var route = 'endpoint/' + endpointName;
                        customRequest('POST', route, {model: model}, params, cb);
                    },
                    put: function (endpointName, model, params, cb) {
                        var route = 'endpoint/' + endpointName;
                        customRequest('PUT', route, {model: model}, params, cb);
                    },
                    delete: function (endpointName, params, cb) {
                        var route = 'endpoint/' + endpointName;
                        customRequest('DELETE', route, null, params, cb);
                    }
                };
            }
        ]
    );

    customEndpoint.directive(
        'servercustomEndpointLink',
        [
            'server-custom-settings',
            'server-custom-auth',
            '$rootScope',
            function (customSettings, Auth, $rootScope) {
                return {
                    restrict: 'A',
                    scope: {},
                    link: function (scope, elem, attrs) {
                        var endpointName = attrs.servercustomEndpointLink,
                            url;

                        function changeUrl(token) {
                            url = customSettings.customUrl + '/endpoint/' + endpointName +
                                (endpointName.indexOf('?') < 0 ? '?' : '&') + 'access_token=' + token || '';
                            elem.attr('href', url);
                        }

                        Auth.getUserInfo().then(function (user) {
                            changeUrl(user.authToken);
                        });

                        $rootScope.$on('auth-set-token', function (event, token) {
                            changeUrl(token);
                        });

                        $rootScope.$on('auth-logout', function (event, data) {
                            changeUrl();
                        });
                    }
                };
            }
        ]
    );
});