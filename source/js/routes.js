/**
 * Defines the main routes in the application.
 * The routes you see here will be anchors '#/' unless specifically configured otherwise.
 */

define(['app', 'config'], function (app) {
    'use strict';
    app.config(function ($routeProvider) {
        $routeProvider.when('/home', {
            templateUrl: 'partials/home.html',
            controller: 'homeCtrl'
        });

        $routeProvider.when('/user/login', {
            templateUrl: 'partials/login.html',
            controller: 'loginCtrl'
        });

        $routeProvider.when('/user/register', {
            templateUrl: 'partials/register.html',
            controller: 'registerCtrl'
        });
        
        $routeProvider.when('/contact', {
            templateUrl: 'partials/contact.html',
            controller: 'contactCtrl'
        });

        $routeProvider.otherwise({
            redirectTo: '/'
        });

        //$locationProvider.html5Mode(true);
    });
});
